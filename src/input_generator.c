// This file is part of PRTest,
// a random test generator for C:
// https://gitlab.com/sosy-lab/software/prtest
//
// SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

#include <math.h>
#include <sanitizer/coverage_interface.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "random_tester.h"

#define FIXED_SEED 1618033988

unsigned int test_size = 0;
static _Bool rand_initialized = 0;

_Bool test_is_new = 0;
char test_vector[MAX_TEST_SIZE + 1][100];

unsigned int get_rand_seed() {
  if (rand_initialized) {
    srand(get_rand_seed());
  }
#ifdef FIXED_SEED
  return FIXED_SEED;
#else
  struct timespec curr_time;
  clock_gettime(CLOCK_REALTIME, &curr_time);
  return curr_time.tv_nsec;
#endif
}

void __prtest_input(void *var, size_t var_size, const char *var_name) {
  int inp_size = var_size * sizeof(char) * 2 + 1;
  char input_val[inp_size];
  unsigned char *new_val = malloc(sizeof(char) * var_size);
  memset(input_val, 0, inp_size);
  for (int i = 0; i < var_size; i++) {
    new_val[var_size - i - 1] = (char)(rand() & 255);
    char *current_pos = &input_val[i * 2];
    snprintf(current_pos, 3, "%.2x", new_val[var_size - i - 1]);
  }
  snprintf(test_vector[test_size], 99, "0x%s", input_val);
  memcpy(var, new_val, var_size);
  free(new_val);

  test_size++;

  if (test_size > MAX_TEST_SIZE) {
    fprintf(stderr, "Maximum test vector size of %d reached, aborting.\n",
            MAX_TEST_SIZE);
    abort();
  }
}

void reset_test_vector() {
  for (int i = 0; i < MAX_TEST_SIZE && test_vector[i][0] != 0; i++) {
    memset(test_vector[i], 0, 1);
  }
  test_size = 0;
}

void __sanitizer_cov_trace_pc_guard_init(uint32_t *start, uint32_t *stop) {
  static uint64_t N; // Counter for the guards.
  if (start == stop || *start)
    return; // Initialize only once.
  for (uint32_t *x = start; x < stop; x++)
    *x = ++N; // Guards should start from 1.
}

void __sanitizer_cov_trace_pc_guard(uint32_t *guard) {
  if (!*guard) {
    return;
  }

  *guard = 0;
  test_is_new = 1;
}
