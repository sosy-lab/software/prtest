// This file is part of PRTest,
// a random test generator for C:
// https://gitlab.com/sosy-lab/software/prtest
//
// SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

#include <math.h>
#include <setjmp.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

#include "input_generator.c"
#include "random_tester.h"

#define VERSION "2.0"

#define METADATA_TEMPLATE                                                      \
  "<?xml version='1.0' encoding='UTF-8' standalone='no'?>\n"                   \
  "<!DOCTYPE test-metadata PUBLIC \"+//IDN sosy-lab.org//DTD test-format "     \
  "test-metadata 1.1//EN\" "                                                   \
  "\"https://sosy-lab.org/test-format/test-metadata-1.1.dtd\">\n"              \
  "<test-metadata>\n"                                                          \
  "  <sourcecodelang>C</sourcecodelang>\n"                                     \
  "  <producer>PRTest %s</producer>\n"                                         \
  "  <specification>COVER( init(main()), FQL(COVER EDGES(@DECISIONEDGE)) "     \
  ")</specification>\n"                                                        \
  "  <programfile>%s</programfile>\n"                                          \
  "  <programhash>%s</programhash>\n"                                          \
  "  <entryfunction>main</entryfunction>\n"                                    \
  "  <architecture>32bit</architecture>\n"                                     \
  "  <creationtime>%s</creationtime>\n"                                        \
  "</test-metadata>"
#define METADATA_NAME "metadata.xml"

#define TEST_TEMPLATE                                                          \
  "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"             \
  "<!DOCTYPE testcase PUBLIC \"+//IDN sosy-lab.org//DTD test-format testcase " \
  "1.1//EN\" \"https://sosy-lab.org/test-format/testcase-1.1.dtd\">\n"         \
  "<testcase>\n"                                                               \
  "%s"                                                                         \
  "</testcase>"

#define TEST_INPUT_TEMPLATE "<input>%s</input>\n"

#define MAX_TEST_NUMBER 150000
#define TEST_DIR "test-suite"

// Number of generated, meaningful tests
static unsigned int test_runs = 0;
// Number of program runs that tried to produce meaningful tests
static unsigned long long total_runs = 0;

static int done = 0;

extern int __main(void);
void write_test();
void reset_test();
jmp_buf env;

// destination should be at least strlen(TEST_DIR) + strlen(basename) + 1
void get_path(char *dest, const char *basename) {
  sprintf(dest, "%s/%s", TEST_DIR, basename);
}

int get_path_size(const char *basename) {
  return strlen(basename) + strlen(TEST_DIR) + 2;
}

// Given target should have a size of 800 or bigger, to be on the safe side
void get_metadata(char *target, const char *program_name) {
  char metadata[800];
  // two digits for each part of semantic versioning plus two dots,
  // e.g., 13.0.20 // (3 * 2 + 2)
  // + "v" in beginning + "\0"
  char version[10];
  sprintf(version, "v%s", VERSION);
  char hash[65] = "null";
  char time_str[24];
  time_t t;
  time(&t);
  struct tm *time_info = localtime(&t);
  strftime(time_str, 24, "%Y-%m-%d %H:%M:%S%Z", time_info);

  sprintf(metadata, METADATA_TEMPLATE, version, program_name, hash, time_str);
  strcpy(target, metadata);
}

void write_metadata(const char *program_name) {
  char metadata[800];
  get_metadata(metadata, program_name);
  char file_target[get_path_size(METADATA_NAME)];
  get_path(file_target, METADATA_NAME);

  FILE *metadata_file = fopen(file_target, "w");
  fprintf(metadata_file, "%s\n", metadata);
  fclose(metadata_file);
}

// Return string of testcase xml for current test case
void get_test_xml(char *dest) {
  const unsigned int max_input_line_size = strlen(TEST_INPUT_TEMPLATE) + 32;
  char input_lines[test_size * max_input_line_size + 1];
  strcpy(input_lines, ""); // make sure its empty, for strcat below
  for (int i = 0; test_vector[i][0] != '\0'; i++) {
    char line[max_input_line_size + 1];

    // TEST_INPUT_TEMPLATE already contains newline, so we can just add the
    // input value
    sprintf(line, TEST_INPUT_TEMPLATE, test_vector[i]);
    strcat(input_lines, line);
  }
  sprintf(dest, TEST_TEMPLATE, input_lines);
}

void write_test() {
  char test_xml[test_size * 49 + strlen(TEST_TEMPLATE) + 1];
  get_test_xml(test_xml);

  unsigned int digits_needed = log10(test_runs + 1) + 1;
  // enough space to write vector%u.xml with `digits_needed` digits
  char file_name[10 + 1 + digits_needed];
  sprintf(file_name, "vector%u.xml", test_runs);
  //  enough space to concatenate TEST_DIR with '/' and vector%u.test and \0
  char file_target[strlen(TEST_DIR) + strlen(file_name) + 1];
  get_path(file_target, file_name);

  FILE *vector = fopen(file_target, "w");
  fprintf(vector, "%s\n", test_xml);
  fclose(vector);
  test_runs++;
}

void abort_handler(int sig) { longjmp(env, 1); }

void exit_handler(int status, void *nullarg) {
  if (done) {
    printf("\nNumber of program executions: %llu\n", total_runs);
    printf("Number of created tests: %u\n", test_runs);
    exit(0);
  } else {
    on_exit(exit_handler, NULL);
    longjmp(env, 1);
  }
}

void exit_gracefully(int sig) {
  done = 1;
  exit(-sig);
}

int main(int argc, char *argv[]) {
  if (argc > 2) {
    printf("Expected at most one argument");
    exit(1);
  }
  mkdir(TEST_DIR, 0777);

  char *program_name;
  if (argc == 2) {
    program_name = argv[1];
  } else {
    program_name = "unknown";
  }
  write_metadata(program_name);

  signal(SIGINT, exit_gracefully);
  signal(SIGTERM, exit_gracefully);
  signal(SIGABRT, abort_handler);
  on_exit(exit_handler, NULL);

  while (test_runs < MAX_TEST_NUMBER) {
    reset_test();
    if (setjmp(env) == 0) {
      total_runs++;
      __main();
    }
    if (test_is_new) {
      write_test();
    }
  }
  done = 1;
  exit(0);
}

void reset_test() {
  reset_test_vector();
  test_is_new = 0;
}
