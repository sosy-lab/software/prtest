// This file is part of PRTest,
// a random test generator for C:
// https://gitlab.com/sosy-lab/software/prtest
//
// SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

#include "random_tester.h"

void __VERIFIER_error() { exit(1); }

void __VERIFIER_assume(int cond) {
  if (!cond) {
    abort();
  }
}

char __VERIFIER_nondet_char() {
  char val;
  __prtest_input(&val, sizeof(char), "val");
  return val;
}

unsigned char __VERIFIER_nondet_uchar() {
  char val;
  __prtest_input(&val, sizeof(char), "val");
  return val;
}

short __VERIFIER_nondet_short() {
  short val;
  __prtest_input(&val, sizeof(short), "val");
  return val;
}

unsigned short __VERIFIER_nondet_ushort() {
  unsigned short val;
  __prtest_input(&val, sizeof(unsigned short), "val");
  return val;
}

int __VERIFIER_nondet_int() {
  int val;
  __prtest_input(&val, sizeof(int), "val");
  return val;
}

unsigned int __VERIFIER_nondet_uint() {
  unsigned int val;
  __prtest_input(&val, sizeof(unsigned int), "val");
  return val;
}

long __VERIFIER_nondet_long() {
  long val;
  __prtest_input(&val, sizeof(long), "val");
  return val;
}

unsigned long __VERIFIER_nondet_ulong() {
  unsigned long val;
  __prtest_input(&val, sizeof(unsigned long), "val");
  return val;
}

long long __VERIFIER_nondet_longlong() {
  long long val;
  __prtest_input(&val, sizeof(long long), "val");
  return val;
}

unsigned long long __VERIFIER_nondet_ulonglong() {
  unsigned long long val;
  __prtest_input(&val, sizeof(unsigned long long), "val");
  return val;
}

float __VERIFIER_nondet_float() {
  float val;
  __prtest_input(&val, sizeof(float), "val");
  return val;
}

double __VERIFIER_nondet_double() {
  double val;
  __prtest_input(&val, sizeof(double), "val");
  return val;
}

_Bool __VERIFIER_nondet_bool() { return (_Bool)__VERIFIER_nondet_int(); }

void *__VERIFIER_nondet_pointer() { return (void *)__VERIFIER_nondet_ulong(); }

unsigned int __VERIFIER_nondet_size_t() { return __VERIFIER_nondet_uint(); }

unsigned char __VERIFIER_nondet_u8() { return __VERIFIER_nondet_uchar(); }

unsigned short __VERIFIER_nondet_u16() { return __VERIFIER_nondet_ushort(); }

unsigned int __VERIFIER_nondet_u32() { return __VERIFIER_nondet_uint(); }

unsigned int __VERIFIER_nondet_U32() { return __VERIFIER_nondet_u32(); }

unsigned char __VERIFIER_nondet_unsigned_char() {
  return __VERIFIER_nondet_uchar();
}

unsigned int __VERIFIER_nondet_unsigned() { return __VERIFIER_nondet_uint(); }
unsigned int __VERIFIER_nondet_unsigned_int() {
  return __VERIFIER_nondet_uint();
}
