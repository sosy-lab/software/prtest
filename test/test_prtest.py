# This file is part of PRTest,
# a random test generator for C:
# https://gitlab.com/sosy-lab/software/prtest
#
# SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import os
import psutil
import subprocess
import time
import signal


ROOT_DIR = os.path.join(os.path.dirname(__file__), "..")
PRTEST = os.path.join(ROOT_DIR, "bin", "prtest")
TEST_DIR = os.path.join(ROOT_DIR, "test")

PRTEST_TIMEOUT = 3  # seconds


def setUp():
    assert os.path.exists(ROOT_DIR)
    assert os.path.exists(PRTEST)
    assert os.path.exists(TEST_DIR)


def test_multifile():
    prog_1 = os.path.join(TEST_DIR, "multifile1.c")
    prog_2 = os.path.join(TEST_DIR, "multifile2.c")
    try:
        subprocess.run([PRTEST, prog_1, prog_2], timeout=PRTEST_TIMEOUT)
        assert False  # shouldn't be reached

    except subprocess.TimeoutExpired:
        pass  # expected


def test_stdin():
    stdin_program = os.path.join(TEST_DIR, "stdin.c")
    try:
        subprocess.check_call([PRTEST, stdin_program], timeout=PRTEST_TIMEOUT)
        assert False, "Expected execution to fail"
    except subprocess.CalledProcessError as e:
        pass  # expected
    except subprocess.TimeoutExpired as e:
        assert False, "Expected execution to fail before timeout"


def test_stdout():
    stdout_program = os.path.join(TEST_DIR, "output.c")
    with psutil.Popen(
        [PRTEST, stdout_program], stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    ) as prtest_process:
        try:
            output, _ = prtest_process.communicate(timeout=PRTEST_TIMEOUT)
        except subprocess.TimeoutExpired:
            _kill_children(prtest_process)
            output, _ = prtest_process.communicate()
            assert len(output) < 100, "Output has more than 100 lines, looks like stdout or stdin are going through"

def _kill_children(parent):
    procs = parent.children(recursive=True) + [parent]
    for p in procs:
        p.terminate()
    gone, alive = psutil.wait_procs(procs, timeout=1)
    for p in alive:
        p.kill()
