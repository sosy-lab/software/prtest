// This file is part of PRTest,
// a random test generator for C:
// https://gitlab.com/sosy-lab/software/prtest
//
// SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

#include<stdio.h>
extern int __VERIFIER_nondet_int();


int main() {
  int n = __VERIFIER_nondet_int();

  for (int i = 0; i < n; i++) {
    printf("stdout\n");
    fprintf(stderr, "stderr\n");
  }

  if (n > 0) {
    return 0;
  } else {
    return 1;
  }
}
