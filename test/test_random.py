# This file is part of PRTest,
# a random test generator for C:
# https://gitlab.com/sosy-lab/software/prtest
#
# SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import os
import numpy as np
import subprocess
import scipy.stats as stats

binary_dir = os.path.join(os.path.dirname(__file__), "../build/test")
print_inputs = os.path.join(binary_dir, "print_inputs")
check_inputs = os.path.join(binary_dir, "check_inputs")


def setUp():
    assert print_inputs is not None
    assert check_inputs is not None


def test_generationAndParsing_consistent():
    expected = subprocess.check_output([print_inputs])

    actual = subprocess.check_output([check_inputs], input=expected)

    assert expected == actual


def test_generation_IntegersDistributedEvenly():
    confidence_value = 0.05
    expected_distribution = stats.uniform(loc=-2**31, scale=2*(2**31)-1).cdf

    generated_data = subprocess.check_output([print_inputs])
    generated_data = generated_data.split(b'\n')[:-1]  # skip last, empty element
    generated_data = np.array([int(a) for a in generated_data])
    kstest_p_value = stats.kstest(generated_data, expected_distribution)[1]

    _visualize_data(generated_data)
    assert kstest_p_value > confidence_value




def _visualize_data(data):
    # Taken from https://stackoverflow.com/questions/20295646/python-ascii-plots-in-terminal
    gnuplot = subprocess.Popen(["gnuplot"],
                           stdin=subprocess.PIPE)
    gnuplot.stdin.write(b"set term dumb 79 15\n")
    gnuplot.stdin.write(b"plot '-' using 1:2 title 'Line1' with linespoints \n")
    for i,j in zip(range(0, len(data)), data):
       gnuplot.stdin.write(b"%f %f\n" % (i,j))
    gnuplot.stdin.write(b"e\n")
    gnuplot.stdin.flush()

