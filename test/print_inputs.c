// This file is part of PRTest,
// a random test generator for C:
// https://gitlab.com/sosy-lab/software/prtest
//
// SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

#include<stdio.h>

#include "random_tester.h"

#define DEBUG 1


int main() {
  int x[MAX_TEST_SIZE];
  for (int i = 0; i < MAX_TEST_SIZE; i++) {
    __prtest_input(&(x[i]), sizeof(x[i]), "x");
    printf("%d\n", x[i]);
  }
}
