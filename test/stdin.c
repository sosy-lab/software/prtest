// This file is part of PRTest,
// a random test generator for C:
// https://gitlab.com/sosy-lab/software/prtest
//
// SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

#include<stdio.h>
#include<stdlib.h>

extern int __VERIFIER_nondet_int();

int main() {
  char * x = malloc(100 * sizeof(char));
  gets(x);

  int i = __VERIFIER_nondet_int();
  if (i > 0) {
    return 0;
  } else {
    return 1;
  }
}
