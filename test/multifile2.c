// This file is part of PRTest,
// a random test generator for C:
// https://gitlab.com/sosy-lab/software/prtest
//
// SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

int __VERIFIER_nondet_int(void);
int foobar(void);

int main() {
  int x = foobar();
  int y = __VERIFIER_nondet_int();

  if (y > x) {
    return 1;
  } else {
    return 0;
  }
}
