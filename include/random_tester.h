// This file is part of PRTest,
// a random test generator for C:
// https://gitlab.com/sosy-lab/software/prtest
//
// SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

#include <stdlib.h>

#define TESTER_HEADER
#define MAX_TEST_SIZE 10000

extern void __prtest_input(void *var, size_t var_size, const char *var_name);
extern void reset_test_vector(void);
