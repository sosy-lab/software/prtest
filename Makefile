# This file is part of PRTest,
# a random test generator for C:
# https://gitlab.com/sosy-lab/software/prtest
#
# SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

TEST_DIR = build/test
TEST_SRC = test
SRC_DIR = src
INCLUDE_DIR = include
CC = clang

all: test

$(TEST_DIR)/print_inputs: $(TEST_DIR) $(SRC_DIR)/random_tester.c $(TEST_SRC)/print_inputs.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -I$(INCLUDE_DIR) -o $(TEST_DIR)/print_inputs $(SRC_DIR)/input_generator.c $(TEST_SRC)/print_inputs.c

$(TEST_DIR)/check_inputs: $(TEST_DIR) $(TEST_SRC)/check_inputs.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $(TEST_DIR)/check_inputs $(TEST_SRC)/check_inputs.c

test: clean-vector $(TEST_DIR)/print_inputs $(TEST_DIR)/check_inputs
	pytest test/test_*py --capture=no

$(TEST_DIR):
	mkdir -p $(TEST_DIR)

clean: clean-vector
	rm -f $(TEST_DIR)

clean-vector:
	rm -f vector.test
