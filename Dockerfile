# This file is part of PRTest,
# a random test generator for C:
# https://gitlab.com/sosy-lab/software/prtest
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: CC0-1.0

FROM ubuntu:22.04
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
  gcc-multilib \
  clang \
  python3-pip \
  gnuplot \
  && rm -rf /var/lib/apt/lists/*

ADD requirements.txt requirements.txt
RUN python3 -m pip install -r requirements.txt \
  && rm requirements.txt
