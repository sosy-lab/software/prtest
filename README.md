<!--
This file is part of PRTest,
a random test generator for C:
https://gitlab.com/sosy-lab/software/prtest

SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# PRTest, the Plain Random Tester.

[![Apache 2.0 License](https://img.shields.io/badge/license-Apache--2-brightgreen.svg?style=flat)](https://www.apache.org/licenses/LICENSE-2.0)

PRTest consists of a C test-generation harness that is compiled against
the program under test, and an execution engine that repeatedly
executes the test-generation harness to create new test cases
and filters the ones covering new code (based on block coverage on the LLVM IR level).

## Dependencies

PRTest requires clang 3.9 or later.

Test requirements:

- python3
- modules listed in requirements.txt (`pip3 install -r requirements.txt`)
- gnuplot (`apt install gnuplot`)

## Usage

`bin/prtest [compiler_arguments] <program_files>`

Example usage: `bin/prtest test/multifile1.c test/multifile2.c`

Compiler arguments and program files can also be mixed, if necessary.
PRTest passes all given arguments to the clang compiler,
so any compiler argument can be provided.

PRTest will run until interrupted by the user (e.g. with Ctrl+C).
It puts all test cases of relevance into directory `output/test-suite`.

## Development

To run tests, run `make test`.

## License

All file in this project are licensed under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0)
and have copyright by [Dirk Beyer](https://www.sosy-lab.org/people/beyer/).
